<?php

namespace Tests\Unit;

use App\Constants\Titles;
use App\Helpers\Person;
use App\Helpers\PersonHandler;
use App\Helpers\PersonParser;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Tests\TestCase;

class PersonFormatterTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function it_extracts_two_person_objects_from_two_name_string()
    {
        $names = implode(' and ', [
            $this->generateFullName(),
            $this->generateFullName(),
        ]);
        $people = (new PersonHandler($names))->handle();

        $this->assertIsArray($people);
        $this->assertIsArray(current($people));
        $this->assertCount(2, $people);
    }

        /** @test */
        public function it_extracts_two_person_objects_from_two_name_string_with_ampersand_delimiter()
        {
            $names = implode(' & ', [
                $this->generateFullName(),
                $this->generateFullName(),
            ]);
            $people = (new PersonHandler($names))->handle();
    
            $this->assertIsArray($people);
            $this->assertIsArray(current($people));
            $this->assertCount(2, $people);
        }

    /** @test */
    public function it_extracts_two_person_objects_correctly_when_one_name_contains_and()
    {
        $names = implode(' and ', [
            $this->generateFullName([
                'last_name' => 'Mandara',
            ]),
            $this->generateFullName(),
        ]);
        $people = (new PersonHandler($names))->handle();
        
        $this->assertIsArray($people);
        $this->assertIsArray(current($people));
        $this->assertCount(2, $people);
    }

    /** @test */
    public function it_extracts_two_person_objects_correctly_when_full_name_is_title()
    {
        $people = (new PersonHandler('Mr and Mrs Smith'))->handle();

        $this->assertIsArray($people);
        $this->assertIsArray(current($people));
        $this->assertCount(2, $people);
        $this->assertEquals(current($people)['title'], 'Mr');
        $this->assertEquals(current($people)['last_name'], 'Smith');
        $this->assertEquals(end($people)['title'], 'Mrs');
        $this->assertEquals(end($people)['last_name'], 'Smith');
    }

    /** @test */
    public function it_extracts_two_person_objects_correctly_when_full_name_is_title_and_delimiter_is_ampersand()
    {
        $people = (new PersonHandler('Mr & Mrs Smith'))->handle();
        
        $this->assertIsArray($people);
        $this->assertIsArray(current($people));
        $this->assertCount(2, $people);
        $this->assertEquals(current($people)['title'], 'Mr');
        $this->assertEquals(current($people)['last_name'], 'Smith');
        $this->assertEquals(end($people)['title'], 'Mrs');
        $this->assertEquals(end($people)['last_name'], 'Smith');
    }

    /** @test */
    public function it_extracts_title_from_full_name()
    {
        $fullName = $this->generateFullName([
            'title' => Titles::TITLE_MR,
        ]);
        $people = (new PersonHandler($fullName))->handle();
        $person = current($people);

        $this->assertEquals($person['title'], Titles::TITLE_MR);
    }

    /** @test */
    public function it_extracts_initial_from_full_name()
    {
        $initial = 'S';
        $fullName = $this->generateFullName([
            'first_name' => $initial,
        ]);
        $people = (new PersonHandler($fullName))->handle();
        $person = current($people);

        $this->assertEquals($person['initial'], 'S');
        $this->assertNull($person['first_name']);
    }

    /** @test */
    public function it_extracts_initial_with_dot_from_full_name()
    {
        $initial = 'S.';
        $fullName = $this->generateFullName([
            'first_name' => $initial,
        ]);
        $people = (new PersonHandler($fullName))->handle();
        $person = current($people);

        $this->assertEquals($person['initial'], 'S');
        $this->assertNull($person['first_name']);
    }

    /** @test */
    public function it_extracts_first_name_from_full_name()
    {
        $firstName = $this->faker->firstName;
        $fullName = $this->generateFullName([
            'first_name' => $firstName,
        ]);
        $people = (new PersonHandler($fullName))->handle();
        $person = current($people);

        $this->assertEquals($person['first_name'], $firstName);
    }

    /** @test */
    public function it_extracts_last_name_from_full_name()
    {
        $lastName = $this->faker->lastName;
        $fullName = $this->generateFullName([
            'last_name' => $lastName,
        ]);
        $people = (new PersonHandler($fullName))->handle();
        $person = current($people);

        $this->assertEquals($person['last_name'], $lastName);
    }

    /** @test */
    public function it_extracts_double_barrel_last_name_from_full_name()
    {
        $lastName = 'Smith-Barnes';
        $fullName = $this->generateFullName([
            'last_name' => $lastName,
        ]);
        $people = (new PersonHandler($fullName))->handle();
        $person = current($people);

        $this->assertEquals($person['last_name'], $lastName);
    }

    /**
     * Generate a full name with salutation.
     *
     * @return string
     */
    protected function generateFullName(array $overrides = []): string
    {
        return sprintf(
            '%s %s %s',
            $overrides['title'] ?? $this->getRandomTitle(),
            $overrides['first_name'] ?? $this->faker->firstName,
            $overrides['last_name'] ?? $this->faker->lastName
        );
    }

    /**
     * Generate title from available CSV titles.
     *
     * @return string
     */
    protected function getRandomTitle(): string
    {
        return Titles::TITLE_MAP[array_rand(Titles::TITLE_MAP)];
    }
}
