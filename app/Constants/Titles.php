<?php

namespace App\Constants;

class Titles
{
    const TITLE_MR = 'Mr';
    const TITLE_MRS = 'Mrs';
    const TITLE_MISTER = 'Mister';
    const TITLE_DR = 'Dr';
    const TITLE_MS = 'Ms';
    const TITLE_PROF = 'Prof';

    const TITLE_MAP = [
        self::TITLE_MR,
        self::TITLE_MRS,
        self::TITLE_MISTER,
        self::TITLE_DR,
        self::TITLE_MS,
        self::TITLE_PROF,
    ];
}