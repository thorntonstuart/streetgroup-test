<?php

namespace App\Http\Controllers;

use App\Helpers\PersonHandler;
use Illuminate\Http\Request;
use League\Csv\Reader;
use League\Csv\Statement;

class PeopleController extends Controller
{
    public function index()
    {
        $csv = Reader::createFromPath(storage_path('app/examples.csv'));
        $csv->setHeaderOffset(0);

        $records = (new Statement())->process($csv);

        $people = collect($records)->map(function (array $record) {
            return (new PersonHandler($record['homeowner']))->handle();
        })->toArray();

        return view('people.index', [
            'people' => $people
        ]);
    }
}
