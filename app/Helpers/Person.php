<?php

namespace App\Helpers;

use App\Constants\Titles;

class Person
{
    /**
     * @var string
     */
    protected $fullName;

    /**
     * @var string
     */
    protected $nameParts;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $firstName;
    
    /**
     * @var string
     */
    protected $initial;

    /**
     * @var string
     */
    protected $lastName;

    public function __construct(string $fullName)
    {
        $this->fullName = $fullName;
        $this->nameParts = explode(' ', $this->fullName);
    }

    /**
     * Return built Person object
     *
     * @return Person
     */
    public function parse(): array
    {
        $this->setTitle();

        if (count($this->nameParts) > 2) {
            $this->setInitial();
            $this->setFirstName();
        }

        $this->setLastName();

        return [
            'title' => $this->getTitle(),
            'initial' => $this->getInitial(),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
        ];
    }

    /**
     * Get fullName property
     *
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * Set title property
     *
     * @return void
     */
    protected function setTitle(): void
    {
        $this->title = collect(Titles::TITLE_MAP)->filter(
            function (string $title, int $key) {
                return $title === $this->nameParts[0];
            }
        )->first();
    }

    /**
     * Get title property
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set firstName property
     *
     * @return void
     */
    protected function setFirstName(): void
    {
        if ($this->checkStringIsInitial($this->nameParts[1])) {
            return;
        }

        $this->firstName = $this->nameParts[1];
    }

    /**
     * Get firstName property
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set initial property
     *
     * @return void
     */
    protected function setInitial(): void
    {
        $this->initial = $this->checkStringIsInitial($this->nameParts[1]) ?: null;
    }

    /**
     * Get initial property
     *
     * @return string|null
     */
    public function getInitial(): ?string
    {
        return $this->initial;
    }

    /**
     * Set lastName property
     *
     * @return void
     */
    protected function setLastName(): void
    {
        $this->lastName = end($this->nameParts);
    }

    /**
     * Get lastName property
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Check if string matches regex for initials
     *
     * @param string $string
     * @return boolean
     */
    protected function checkStringIsInitial(string $string)
    {
        return preg_match('/^([A-Z]\.|[A-Z]{1})+$/', $string, $match) ? str_replace('.', '', current($match)) : false;
    }
}