<?php

namespace App\Helpers;

use App\Constants\Titles;
use Illuminate\Support\Collection;

class PersonHandler
{
    /**
     * @var string
     */
    protected $nameString;

    /**
     * @var array
     */
    protected $names = [];

    public function __construct(string $nameString)
    {
        $this->nameString = $nameString;
    }

    public function handle(): array
    {
        $this->checkForMultipleNames();

        return array_map(function (string $name) {
            return (new Person($name))->parse();
        }, $this->names);
    }

    protected function checkForMultipleNames()
    {
        if (preg_match('/.+?(?=\s(?>and|&)?\s(.*))/', $this->nameString)) {
            preg_match('/.+?(?=\s(?>and|&)?\s(.*))/', $this->nameString, $this->names);

            foreach ($this->names as $index => $name) {
                if (in_array($name, Titles::TITLE_MAP)) {
                    $finalNameFull = explode(' ', end($this->names));
                    $nameSlice = in_array($finalNameFull[0], Titles::TITLE_MAP)
                        ? implode(' ', array_slice($finalNameFull, 1))
                        : end($finalNameFull);
                    
                    $completeName = sprintf('%s %s', $name, $nameSlice);
                    $this->names[$index] = $completeName;
                }
            }

            return $this->names;
        }

        return array_push($this->names, $this->nameString);
    }
}